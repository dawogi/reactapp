import React  from 'react';
import './Person.css';

const person = (props) => {
    return(
        /*
        We want to change the name in App from Person component.
        We add new input and whatever is entered here it will be used as new name in App component.
        For that we listen for special event onChange, it will change whenever the value in input changes.
        React execute method which will be pass down from app.js file.

        Two way binding - When we change it we want to propagate that change so we can update the state
        but we also want to see current value of the input - To do this we set value={props.name}.
        */
       
        <div className="Person">
            <p onClick={props.click}>I'm {props.name} and I am {props.age} years old!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}/>
        </div>
    )
}

export default person;