import React, { Component } from 'react';
import Person from './Person/Person';
import './App.css';

class App extends Component {
  state = {
    persons: [
      {id: '1', name: "Max", age: 12},
      {id: '2', name: "Mery", age: 30},
      {id: '3', name: "Bob", age: 25}
    ]
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.find(p =>{
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({person: persons})
  }

  deletePersonHandler = (personIndex) =>{
    //We need to create a copy of array to operate on a copy.
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons})
  }

  /*
  We bound onChange to prop.changed in Person.js which holds reference to nameChangedHandler. 
  Default event object in nameChangedHandler is used to extract the target which is the input element
  and then value of the target which is what we eneterd(event.target.value).
  This is show how we dynamically update something by dynamically call a event and pass down event reference or
  method reference.This also shows how we can handle inputs.
  */
 
  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow});
  }

  render() {

     //This is JS object that will be css representation for inline styling of a button bellow
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };


    /*
      When React render/rerender something to the screen it execures render method not just return method. 
      We can take advantage of this to display content conditionally.
    */
    let persons = null;

    if (this.state.showPersons){
      persons = (
        <div >
          {this.state.persons.map((person, index) => {
            return <Person 
              click={() => this.deletePersonHandler(index)}

              name={person.name} 
              age={person.age}
              key={person.id} 
              changed={(event) => this.nameChangedHandler(event, person.id)} />
          })}
        </div>
      );
    }

    return ( 
      <div className="App">
        <h1>Hi, I'm React App</h1>
        <p>This is really working!</p>
        <button 
          style={style}
          onClick={this.togglePersonsHandler}>Switch</button>
          {persons}
      </div>
    );
  } 
}

export default App;
